# Smart Lamp Demo
A simple smart lamp demo application and Node-RED integration.

## Description
To demonstrate the use of Oniro and the potential integrations with IoT automation platforms/services a simple smart lamp demo is provided with Hackathon resources.
This demo is using Oniro Linux flavor and does the simple task of switching on and off one LED via GPIO pin.

This demo consists of two parts:
1. **Embedded system OS and logic:** Oniro represents a distributed open source operating system for consumer devices that can be flashed on your IoT device. It is the foundation on which to start developing the application logic to interact with the LED.
The sample demo provided comes with a Smart Lamp application and the MQTT Manager that interacts via DBus on target device.

2. **Home automation server:** for this hackathon has been proposed Node-RED as IoT integration platform which acts as a server managing the messages exchange with the IoT device. The demo provided shows how Node-RED can be used to setup an MQTT broker and the logic to trigger actions from a Web Dashboard instantiating an MQTT publisher and subscriber.

Here below an high level detail of the demo's architecture with its building blocks.

![Smart-Lamp-Demo-Arch](./assets/arch.png)


## Getting started

### Dependencies
Please make sure the hardware and software dependencies are available.

### Hardware
* One Raspberry Pi4 board including accessories - https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
* USB to UART adaptor connected to UART pins on RPi4 (for debug)
* One LED with wires connected on GPIO21. This is a 5v pin and will work with an 5v LED without additional resistor.
* One Push Button with wires connected on GPIO5.

### Software
* Access to Hackathon resources on gitlab.com
* Linux build environment. Instructions are verified using Ubuntu 20.04.4 LTS

### Resources
The goal of the hackathon is to create innovative home automation scenarios with devices running Oniro.
To support participants we provide several demo projects and recipes to integrate the projects into Oniro device images.

We have published for you some open repositories that you can clone to extend and to take inspiration to build your own IoT solution.

![Gitlab-Repo](./assets/GitLab.png)

[Here](https://gitlab.com/huawei-iot-innovation-hackathon) on Gitlab has been defined a new public group called *Huawei IoT Innovation Hackathon* with all the resources you will need to carry out the challenge.

The group structure is as follows:

- **Developer Resources:** all the support material you need to replicate the sample demo and to get to know with Oniro and Node-RED.

 - **Documentation:** here you can find guides, tutorials and tips and tricks documents to help you out with the challenge.

   - **smart-lamp-getting-started:** in this repository you will find all the documentation you need to get started with the Smart Lamp demo.

 - **Resources Oniro:** repositories dedicated to Oniro, included the recipes used by Oniro to build the Smart Lamp demo.

   - [meta-dpe-high:](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/meta-dpe-high) the Yocto meta layer that provides recipes targeting Oniro Linux flavor. Here you can find the source code which defines the recipes to be used by *bitbake* to download, decompress, patch, compile, and package all the libraries needed to build Oniro and the Smart Lamp Demo.

   - [mqtt-manager:](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/mqtt-manager) Oniro's service which implements a local bridge from DBus to MQTT and vice-versa.

  - **Sample Project:** repositories dedicated both to Oniro and Node-RED. Here you can find the source code to implement the Smart Lamp Demo on Oniro and to setup your Node-RED instance.

   - [smart-lamp-demo:](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/smart-lamp-demo) repository which includes the Smart Lamp application's source code for Oniro.

   - [smart-lamp-node-red:](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/smart-lamp-node-red/-/tree/main) repository which includes the sources to build a home automation system through the Node-RED platform.


## Replicate Smart Lamp Demo

In order to start replicating the demo you need to follow the two *getting started* guides below:
1. [Building Oniro](/Oniro-Getting-Started.md)
2. [Running Node-RED](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/smart-lamp-node-red/-/blob/main/README.md)

For further help, or general discussion, please use the Hackathon slack team.
