# Oniro - Getting Started

## Building the Smart Lamp Demo
With the Oniro you can build images for different type of devices from the same
source tree by setting the Oniro's build environment.

## Setup the workspace for Oniro
To be able to build an Oniro OS image, before you must install all the required
host packages.<br/>
Here is an example for Ubuntu:
```
sudo apt-get install gawk wget git diffstat unzip texinfo gcc-multilib \
  build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
  xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
  pylint3 xterm file zstd bmap-tools libboost-python1.71-dev
```

To build you own Oniro image, we suggest to use one of the tested distributions:
* Ubuntu 20.04, 19.04, 18.04, 16.04
* Fedora 33, 32, 31, 30
* CentOS 8, 7
* Debian 10, 9, 8
* OpenSUSE Leap 15.2, 15.1

**NB:** If you would build Oniro Kirkstone version the supported distributions
are limited to Ubuntu 20.04 and 18.04.

Windows Subsystem for Linux can be used to build the image. But the build
process will be much slower due to low perfermance on HD access from WSL.

More info on official docs from the [Oniro](https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html#id2)
and [Yocto](https://www.yoctoproject.org/docs/latest/ref-manual/ref-manual.html#required-packages-for-the-build-host)
projects.

## Get the required sources
Oniro files and configs are provided by different Git repos. In order to get all
required repos in a a local directory to use as workspace, you can use the
[git-repo](https://git.ostc-eu.org/OSTC/packaging/git-repo) tool. This tool allows
you to clone different repos in prefixed sub-dirs.

To install git-repo util on Ubuntu you can execute following commands. For others
distribution, please check the Oniro [Repo Workspace](https://docs.oniroproject.org/en/latest/oniro/repo-workspace.html#id2)
page.
```
sudo add-apt-repository ppa:ostc/ppa
sudo apt-get update
sudo apt-get install git-repo
```

Once installed the git-repo, you can initialize the repo workspace and clone all
Oniro's repositories:
```
mkdir $ONIRO_DIR
cd $ONIRO_DIR
repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b dunfell
repo sync --no-clone-bundle
```

In case the last command prompt network errors, you can execute the same
command again until it can fetch all repositories.

Now you can clone the latest required repository that contains the Oniro build
configs to generate the Smart Lamp Demo image.
```
cd $ONIRO_DIR
git clone https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/meta-dpe-high.git
```

## Configure and build the Oniro image 
Oniro build commands must be execute in a terminal with some pre-set environment
vars. The `$ONIRO_DIR/oe-core/oe-init-build-env` help us on setting the required
env vars.
```
cd $ONIRO_DIR
export TEMPLATECONF=../oniro/flavours/linux
source ./oe-core/oe-init-build-env build-oniro
```

The `TEMPLATECONF` var specify the path of a valid Oniro's configs template. That
is a collection of files to use as starting configs to build your Oniro image.
Other valid paths are `../oniro/flavours/zephyr` and `../oniro/flavours/freertos`
that configure your build environment to generate an Oniro image based respectively
on Zephyr or FreeRTOS kernels. __Our demo is based on the `linux` kernel__.<br/>
**NB:** the path can be absolute or relative to the `$ONIRO_DIR/oe-core` dir.

After executing the `oe-core/oe-init-build-env` script you were cd to the
directory specified as last param of the script. In the example we are cd into
the `$ONIRO_DIR/build-oniro`.

The Oniro build system must know where looking for recipes files (instructions
for the build system to compile software to include in the generated image,
those files are grouped by layers).
By default, when you use a provided `TEMPLATECONF`, it already include all basic
layers. In order to be able to build the Smart Lamp Demo you must add also the
`meta-dpe-high` layer to your configs:
```
bitbake-layers add-layer ../meta-dpe-high
```

Previous command will update the `$ONIRO_DIR/build-oniro/conf/bblayers.conf`
file.

Finally, you can build the Oniro image that contains the Smart Lamp Demo. This
operation will take several hours at the first time. During the build process a
lot of source code is downloaded from internet and compiled for the target
machine.
```
export DISTRO=smart-lamp-demo
export MACHINE=raspberrypi4-64
bitbake oniro-image-base
```

In case of fetching error execute latest command again as long as build succeeds.

## Flashing the Image
When you built the Oniro's image (with Raspberry Pi as target machine), you can
flash it on an Micro SD Card and use that Micro SD Card to boot your Raspberry.<br/>
Insert Micro SD Card to your building machine, then check the device node
associated to it. To do it you can use the `lsblk` or `dmesg` tools.<br/>
Commonly the external memories are associated to `/dev/sdX` node.

**NB:** Before flash the Micro SD Card, you must unmount all his partitions.

Those instructions are valid only for Linux machine. Windows Subsystem for
Linux needs other methods to program the microSD card.

```
DEVICE=/dev/sdb
sudo umount $DEVICE*
cd $ONIRO_DIR/build-oniro
sudo bmaptool copy ./tmp/deploy/images/raspberrypi4-64/oniro-image-base-raspberrypi4-64.wic.bz2 $DEVICE
```

**NB:** Be careful not to give wrong device. bmap tool overwrites the content of the given disk.

## Configure the Smart Lamp Demo
The Smart Lamp Demo requires to setup some configs (GPIO, WiFi credentials, MQTT
broker IP...) to define his working environment. Those configs are read from the `appdata`
partition.

The Oniro image, define different partitions mostly of them read-only to increase
the device security. The `appdata` is a writable partition dedicated to device's
applications configs.

To create the configs file, first remove card from reader and put it back. That
will mount all partitions on the Micro SD Card on your building machine. The
`appdata` partition should be mounted under the `/media/USERNAME/appdata` folder,
or use the `mount | grep appdata` to get the mount directory.

Once you finish to create configs file, then you can put the Micro SD Card into
the Raspberry and boot it. After a while, it will be available on your network
and you can remotely login using the following command:

```
ssh root@<RaspberryIP>
```

The default user is `root` without any password.

### MQTT Manager
The [MQTT Manager](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/mqtt-manager/-/blob/main/README.md)
is a MQTT Client that act as a bridge between the local DBus and a specified
MQTT Broker. So to configure this service you must set the broker address and
port.

This application is installed as a Deamon and read his configs from the
`$APPDATA/MQTT/mqtt-manager.conf` file. [Here](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/meta-dpe-high/-/blob/main/recipes-demos/mqtt-manager/files/mqtt-manager.sh)
you can find the MQTT Manager's startup script.

This is an example of the content of the `mqtt-manager.conf` file:
```
[mqtt]
BrokerAddress = 5.196.95.208
BrockerPort = 1883
```

### Smart Lamp
The [Smart Lamp](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/sample-project/smart-lamp-demo/-/blob/main/README.md)
is the software in charge of managing the hardware invovled in the demo (a led
and a push button). It wait/send messages from/to the local DBus.<br/>
Smart Lamp configs require to set the GPIO chip's name (on RaspberryPi you can
use the `gpiochip0`). To list all available GPIO chips on your system you can
use the `gpiodetect` command. Then the GPIOChipLineLed and GPIOChipLineBtn
values must be set according on your HW setup. **NB:** the push button must be
connected to a pull-up GPIO pin (on Raspberry pins from 1 to 8).

This application is installed as a Deamon and read his configs from the
`$APPDATA/SmartLamp/smart-lamp.conf` file. [Here](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/meta-dpe-high/-/blob/main/recipes-demos/smart-lamp-demo/files/smart-lamp-demo.sh)
you can find the Smart Lamp's startup script.

This is an example of the content of the `smart-lamp.conf` file:
```
[common]
GPIOChipName = gpiochip0
GPIOChipLineLed = 21
GPIOChipLineBtn = 5
```

### WiFi Network

The WiFi Auto Configs is a simple script that configure the WiFi network
credentials. This script is also installed as a Daemon, so on each device's
boot, it will configure the WiFi network.

This script looks for the `$APPDATA/WIFI/wifi-auto-config.conf` file to read
WiFi's SSID and password. If the file do not exist, then it will use default
configs (hardcoded in the [script](https://gitlab.com/huawei-iot-innovation-hackathon/developer-resources/resources-oniro/meta-dpe-high/-/blob/main/recipes-demos/wifi-auto-config/files/wifi-auto-config.sh))

This is an example of the content of the `wifi-auto-config.conf` file:
```
SSID="MyWiFi"
PWD="my_password"
```
